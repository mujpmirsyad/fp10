<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::resource('profil', 'ProfilController')->middleware('auth');

Route::get('/beranda', 
    'PertanyaanController@index')
    //function () {return view('adminlte.blank');})  
    ->middleware('auth');

Route::get('/profiles', function () {
    return view('profil.profil2');
})->middleware('auth');

Route::resource('follow', 'FollowController');
Route ::get('/data-tables',function(){
    return view('data-tabel.create');
});

Route::resource('postingan', 'PertanyaanController');

// Route ::get('/postingan/create', 'PertanyaanController@create');

// Route ::post('/postingan', 'PertanyaanController@store');

// Route ::get('/postingan', 'PertanyaanController@index')->middleware('auth');

// Route ::get('/postingan/{id}', 'PertanyaanController@show');

// Route ::get('/postingan/{id}/edit', 'PertanyaanController@edit');

// Route ::put('/postingan/{id}', 'PertanyaanController@update');

// Route ::delete('/postingan/{id}', 'PertanyaanController@destroy');
