<div class="card card-widget widget-user" style="margin-bottom: 0px;">
    <!-- Add the bg color to the header using any of the bg-* classes -->
    <div class="widget-user-header bg-info">
      <h2 class="widget-user-username"><strong>{{Auth::user()->profil->nama}}</strong></h2>
      <p class="widget-user-desc">{{Auth::user()->profil->bio}}</p>
    </div>
    <div class="widget-user-image">
      <img class="img-circle" src="{{ asset('/adminlte/dist/img/user3-128x128.jpg')}}" alt="User Avatar">
    </div>
    <div class="card-footer">
      <div class="row">
        <div class="col-sm-4 border-right">
          <div class="description-block">
            <h5 class="description-header">{{ App\Follow::where('following_id','=',Auth::id())->get()->count()}}</h5>
            <span class="description-text">Following</span>
          </div>
          <!-- /.description-block -->
        </div>
        <!-- /.col -->
        <div class="col-sm-4 border-right">
          <div class="description-block">
            <h5 class="description-header">{{ App\Follow::where('followed_id','=',Auth::id())->get()->count()}}</h5>
            <span class="description-text">Followers</span>
          </div>
          <!-- /.description-block -->
        </div>
        <!-- /.col -->
        <div class="col-sm-4">
          <div class="description-block">
            <h5 class="description-header">ID</h5>
            <span class="description-text">{{Auth::id()}}</span>
          </div>
          <!-- /.description-block -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </div>