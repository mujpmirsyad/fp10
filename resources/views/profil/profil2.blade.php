@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3 mr-3">
      <div class="row justify-content-start"  id="listProfil" >
        @forelse($profil as $profil)
          <div class="col-4 mt-1">
            <div class="card card-widget widget-user">
              <!-- Add the bg color to the header using any of the bg-* classes -->
              <!--<div class="widget-user-header text-white" style="background: url('{{ asset('/adminlte/dist/img/photo1.png')}}') center center;">-->
              <div class="widget-user-header bg-info">
                <h2 class="widget-user-username"><strong>{{$profil -> nama }}</strong></h2>
                <p class="widget-user-desc">{{$profil -> bio }}</p>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="{{ asset('/adminlte/dist/img/avatar5.png')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header">{{ App\Follow::where('following_id','=',$profil->user_id)->get()->count()}}</h5>
                      <span class="description-text">Following</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header">{{ App\Follow::where('followed_id','=',$profil->user_id)->get()->count()}}</h5>
                      <span class="description-text">Followers</span>
                    </div>
                    <!-- /.description-block -->
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4">
                    <form action="{{ route('follow.store', ['id' => $profil->user_id])}}" class="follow-{{ $profil->user_id }}" method="POST">
                      @csrf
                      <input type="submit" value="Follow" class="btn btn-primary btn-sm"></form>
                  </div>
                </div>
                <!-- /.row -->
              </div>
            </div>
          </div>
        @empty
            <p align="center"> Profil tidak ditemukan</p>
        @endforelse

      </div>
    </div>

@endsection