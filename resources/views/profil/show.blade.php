@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4>{{$pertanyaan->judul}}</h4>
        <p>{{$pertanyaan->isi}}</p>
        <p> Penanya : {{ $pertanyaan->penanya->nama_lengkap }} </p>
    </div>
@endsection
