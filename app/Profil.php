<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $guarded = [];

    protected $table = 'profil';

    public function user(){
        return $this->belongsTo('App\User');
    }

    
}
