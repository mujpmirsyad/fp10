<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Postingan; 
use Auth;

class PertanyaanController extends Controller
{
    public function create(){
        return view('postingan.create');
    }

    public function store(Request $request){
            $request->validate([
                //'id' => 'required|unique:postingan',
                'judul' => 'required',
                'isi' => 'required'
            ]);

            //$query = DB::table('pertanyaan')->insert([
              //  "id" => $request["id"],
                //"judul" => $request["judul"],
                //"isi" => $request["isi"],
                //"tanggal_dibuat" => $request["tanggaldibuat"],
                //"tanggal_diperbaharui" => $request["tanggaldiperbaharui"],
                //"profil_id" => $request["profilid"]
            //]);

            // dengan eloquent
            //$pertanyaan = new Pertanyaan;
            //$pertanyaan->id = $request["id"];
            //$pertanyaan->judul = $request["judul"];
            //$pertanyaan->isi = $request["isi"];
            //$pertanyaan->tanggal_dibuat = $request["tanggaldibuat"];
            //$pertanyaan->tanggal_diperbaharui = $request["tanggaldiperbaharui"];
            //$pertanyaan->profil_id = $request["profilid"];
            //$pertanyaan->save();

            // dengan mass asignment
            $postingan = Postingan::create([
                //"id" => $request["id"],
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                // "tanggal_dibuat" => $request["tanggaldibuat"],
                // "tanggal_diperbaharui" => $request["tanggaldiperbaharui"],
                "user_id" => Auth::id()

            ]);

            //dd($postingan);

            return redirect('/postingan')->with('success', 'Postingan Berhasil Disimpan');
    }

    public function index(){
        //$pertanyaans = DB::table('pertanyaan')->get();
        $postingan = Postingan::all();
        return view('postingan.index', compact('postingan'));
    }

    public function show($id) {
        //$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $postingan = Postingan::find($id);
        return view('postingan.show', compact('postingan'));
    }

    public function edit($id){
        $postingan = DB::table('postingan')->where('id', $id)->first();
        return view('postingan.edit', compact('postingan'));
    }

    public function update($id,Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
        
        //$query = DB::table('pertanyaan')
          //          ->where('id', $id)
            //        ->update([
              //          'judul'=>$request['judul'],
                //        'isi'=>$request['isi']
                  //  ]);
        $update = Postingan::where('id', $id)->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                "tanggal_dibuat" => $request["tanggaldibuat"],
                "tanggal_diperbaharui" => $request["tanggaldiperbaharui"],
                "profil_id" => $request["profilid"]
        ]);
        return redirect('/postingan')->with('success', 'Berhasil Update Pertanyaan');
    }

    public function destroy($id) {
        //$query = DB::table('pertanyaan')->where('id', $id)->delete();
        Postingan::destroy($id);
        return redirect('/postingan')->with('success', 'Berhasil Menghapus Pertanyaan');
    }
}
