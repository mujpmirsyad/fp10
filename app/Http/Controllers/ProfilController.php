<?php

namespace App\Http\Controllers;

use App\Profil;
use Illuminate\Http\Request;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;


class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return view('home');

        //$pertanyaan = DB::table('pertanyaan')->get();
        //$profil = Profil::all();
        $profil = Profil::where('user_id','<>',Auth::id())->get();
        
        //$user = Auth::user();

        //$profilid = $user->profile;
        //$pertanyaan = $profilid->pertanyaan->profil_id;
        //$pertanyaan = $profilid->pertanyaan;
        //dd($profil);
        return view('profil.profil2', compact('profil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profil.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:profil',
        ]);

        //dd($request);

        $profil = Profil::create([
            'nama' => $request["nama"],
            'bio' => $request["bio"],
            'user_id' => Auth::id()
        ]);
        //return redirect('/profil')->with('success', 'Profil berhasil disimpan!');
        Alert::success('Selamat bergabung!', 'Anda telah berhasil daftar!');
        return redirect('/beranda');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function show(Profil $profil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function edit(Profil $profil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profil $profil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profil  $profil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profil $profil)
    {
        //
    }
}
