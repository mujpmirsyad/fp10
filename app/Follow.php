<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $table = 'follow';

    protected $guarded = [];
    
    public static function hitungFollowers($id) {
        $followercount = Follow::where('followed_id', $id)->get();
        return $followercount->count();
    }

}
