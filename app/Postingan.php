<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postingan extends Model
{
    protected $table = "postingan";

    //protected $fillable = ["id","judul","isi","yanggal_dibuat","tanggal_diperbaharui","profil_id"];
    protected $guarded = [];

    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
